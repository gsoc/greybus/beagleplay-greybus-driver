obj-m += gb-beagleplay.o

KDIR ?= /lib/modules/$(shell uname -r)/build
PWD := $(CURDIR)

all:
	make -C $(KDIR) M=$(PWD) modules

clean:
	make -C $(KDIR) M=$(PWD) clean

format:
	/bin/clang-format -i gb-beagleplay.c

lsp: clean
	/bin/bear -- make
