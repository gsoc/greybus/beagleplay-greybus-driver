# Introduction
This Linux driver is being developed as a part of Google Summer of Code 2023. It removes the need to use GBridge (which is a userspace application) in the Beagleplay Greybus setup. 

This driver should be used in conjunction with [cc1352-firmware](https://git.beagleboard.org/gsoc/greybus/cc1352-firmware).
